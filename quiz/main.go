package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"os"
)

var nFlag = flag.String("f", "problems.csv", "name of the csv file with questions")

func main() {
	flag.Parse()
	file, err := os.Open(*nFlag)

	if err != nil {
		println(err.Error())
		os.Exit(1)
	}

	r := csv.NewReader(file)
	lines, numberOfLines := initQuiz(r)

	correctAnswers := quiz(lines)

	fmt.Printf("You scored %v out of %v\n", correctAnswers, numberOfLines)

}

func initQuiz(r *csv.Reader) (map[string]string, int) {

	quiz := make(map[string]string)

	numberOfLines := 0

	for {
		record, err := r.Read()

		if err == io.EOF {
			break
		}
		if err != nil {
			os.Exit(2)
		}

		quiz[record[0]] = record[1]
		numberOfLines++

	}

	return quiz, numberOfLines
}

func quiz(lines map[string]string) int {

	correct := 0
	for question, correctAnswer := range lines {
		fmt.Printf("Problem #%v : %v = ", correct+1, question)

		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()

		answer := scanner.Text()

		if answer == correctAnswer {
			correct++
		}
	}

	return correct
}
